# 软件测试上机实验1

3015204342
冀辰阳
软件一班
> 内容：编写三角形判定程序并对其进行Junit测试

### 过程
> 因为我习惯的开发环境是Jetbrains全家桶，因此选用idea + Gradle来进行开发以及测试

环境搭建：在Idea中创建Java的Gradle项目即可。
添加Junit的测试依赖
```groovy
dependencies {
    testCompile group: 'junit', name: 'junit', version: '4.12'
}
```
任何在java和test两个模块中分别写出Triangle和测试类的代码，保持在同一包名下可以避免导包。
详情可见代码部分。

### 测试
**在idea中测试**
在TestTri类中，点击绿色箭头即可测试.
分为 Run, Debug, Run with coverage 三种模式。分别是运行测试，Debug测试和测试并且检测代码覆盖率。

**在命令行中测试**
使用Gradle命令行来执行。因为项目基于强大的Gradle构建系统，所以运行Gradle的test task即可。
```shell
./gradlew test

#如果系统已经配置好gradle环境变量
~/AndroidStudioProjects/softwaretest1
❯ gradle test

BUILD SUCCESSFUL in 0s
3 actionable tasks: 3 up-to-date

```

### 覆盖率 
在idea下可以直接使用 Run with coverage来查看代码覆盖率

![](软件测试以及覆盖率截图.png)

根据idea的显示，代码覆盖了为100% （类覆盖100% 方法覆盖100% 行数覆盖100%）

命令行：可以在Gradle中配置Jacoco插件来运行覆盖率检测操作