package com.twt.test;

public class Triangle {
    public String judge(int a, int b, int c) {

        if (a + b <= c || a + c <= b || b + c <= a) {
            return "Not a triangle";
        } else {
            if (a == b && a == c) {
                return "equilateral";
            } else if (a == c) {
                return "isosceles";
            } else if (b == c) {
                return "isosceles";

            } else if (a == b) {
                return "isosceles";
            } else {
                return "scalene";
            }
        }
    }

}
