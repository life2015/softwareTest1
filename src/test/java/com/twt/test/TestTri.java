package com.twt.test;

import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;


public class TestTri {

    private Triangle triangle;

    @Before
    public void init() {
        triangle = new Triangle();
    }

    @Test
    public void test(){

        assertEquals("equilateral",triangle.judge(1,1,1));

        assertEquals("isosceles",triangle.judge(1,2,2));

        assertEquals("isosceles",triangle.judge(2,1,2));

        assertEquals("isosceles",triangle.judge(2,2,1));

        assertEquals("Not a triangle",triangle.judge(1,2,3));

        assertEquals("scalene",triangle.judge(3,4,5));

    }

}
